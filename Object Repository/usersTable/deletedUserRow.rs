<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>deletedUserRow</name>
   <tag></tag>
   <elementGuidId>3b96cd9a-c604-47f0-b6ea-b8521f872bfe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[4]/td[1]/div/span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//span[text()='Player One']/ancestor::tr)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[4]/td[1]/div/span</value>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>userEmail</name>
   <tag></tag>
   <elementGuidId>f32505e5-7072-425c-b7d9-2c5a70ab36dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[3]/td[2]/div/span/a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tbody/tr[@class=&quot;admin-table-row stat-table-row no-border-row&quot;]/td[2]/div/span/a[starts-with(@href, 'mailto')][contains(text(),'newplayer1@fmsqa.com')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[3]/td[2]/div/span/a</value>
   </webElementProperties>
</WebElementEntity>

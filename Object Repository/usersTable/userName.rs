<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>userName</name>
   <tag></tag>
   <elementGuidId>b80ee286-883e-49e7-922c-6cb0971ce9a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[3]/td[1]/div/span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tbody/tr[@class=&quot;admin-table-row stat-table-row no-border-row&quot;]/td[1]/div/span[contains(text(),'Player One')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[3]/td[1]/div/span</value>
   </webElementProperties>
</WebElementEntity>

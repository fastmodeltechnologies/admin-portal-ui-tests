<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>userFSRole</name>
   <tag></tag>
   <elementGuidId>71c48478-9651-4724-9833-578fadd4fd89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[3]/td[5]/div</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//tbody/tr[@class=&quot;admin-table-row stat-table-row no-border-row&quot;]/td[5]/div[normalize-space()='Player']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div[2]/div/div/main/section/div/div/div/table/tbody/tr[3]/td[5]/div</value>
   </webElementProperties>
</WebElementEntity>

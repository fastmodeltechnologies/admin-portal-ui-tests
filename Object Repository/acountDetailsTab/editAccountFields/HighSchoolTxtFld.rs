<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>HighSchoolTxtFld</name>
   <tag></tag>
   <elementGuidId>3c5f3eb5-8ba4-41a5-afc4-ff330e1aaef9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'newCoachingLevelHighSchool']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>newCoachingLevelHighSchool</value>
   </webElementProperties>
</WebElementEntity>

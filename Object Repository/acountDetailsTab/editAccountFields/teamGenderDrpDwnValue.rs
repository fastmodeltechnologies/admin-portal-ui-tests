<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>teamGenderDrpDwnValue</name>
   <tag></tag>
   <elementGuidId>f37e8987-78c5-4f2f-8fd3-598f8d6afae5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@data-value = '${gender}']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-value = 'Female']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-value</name>
      <type>Main</type>
      <value>Female</value>
   </webElementProperties>
</WebElementEntity>

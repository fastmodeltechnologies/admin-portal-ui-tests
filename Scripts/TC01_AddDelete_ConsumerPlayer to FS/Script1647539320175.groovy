import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration


WebUI.openBrowser('')
WebUI.navigateToUrl(GlobalVariable.URL)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('SignInPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('SignInPage/passwordTxtFld'), GlobalVariable.password)
WebUI.enhancedClick(findTestObject('SignInPage/signINBtn'))
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('accountUsersTab/addUserBtn'))
WebUI.setText(findTestObject('accountUsersTab/addNewUser/newAccountUser/eMailTxtFld'), 'newplayer1@fmsqa.com')
WebUI.setText(findTestObject('accountUsersTab/addNewUser/newAccountUser/firstNameTxtFld'), 'Player')
WebUI.setText(findTestObject('accountUsersTab/addNewUser/newAccountUser/lastNameTxtFld'), 'One')
WebUI.setEncryptedText(findTestObject('accountUsersTab/addNewUser/newAccountUser/passwordTxtFld'), '8SQVv/p9jVTHLrggi8kCzw==')
WebUI.setEncryptedText(findTestObject('accountUsersTab/addNewUser/newAccountUser/confirmPasswordTxtFld'), '8SQVv/p9jVTHLrggi8kCzw==')
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('accountUsersTab/addNewUser/newAccountUser/productSeatDrpDwn'))

if ((GlobalVariable.profile == 'STAGINGQA'))
{
WebUI.doubleClick(findTestObject('accountUsersTab/addNewUser/newAccountUser/productSeatDrpDwnValue',[('rownumber') : 3]))
//WebUI.clickOffset(findTestObject('accountUsersTab/addNewUser/newAccountUser/productSeatDrpDwnValue',[('rownumber') : 3]), 0, -100)
}

if ((GlobalVariable.profile == 'PRODUCTION'))
{
WebUI.enhancedClick(findTestObject('accountUsersTab/addNewUser/newAccountUser/productSeatDrpDwnValue',[('rownumber') : 4]))
WebUI.clickOffset(findTestObject('accountUsersTab/addNewUser/newAccountUser/productSeatDrpDwnValue',[('rownumber') : 4]), 0, -200)
}
		
WebUI.mouseOver(findTestObject('accountUsersTab/addNewUser/newAccountUser/scoutRoleDrpDwn'))
WebUI.waitForElementClickable(findTestObject('accountUsersTab/addNewUser/newAccountUser/scoutRoleDrpDwn'), 10)
WebUI.enhancedClick(findTestObject('accountUsersTab/addNewUser/newAccountUser/scoutRoleDrpDwn'))
WebUI.delay(1)
WebUI.mouseOver(findTestObject('accountUsersTab/addNewUser/newAccountUser/playerRole'))
WebUI.enhancedClick(findTestObject('accountUsersTab/addNewUser/newAccountUser/playerRole'))
WebUI.clickOffset(findTestObject('accountUsersTab/addNewUser/newAccountUser/playerRole'), 0, -300)
WebUI.enhancedClick(findTestObject('accountUsersTab/addNewUser/newAccountUser/saveBtn'))
WebUI.delay(3)
WebUI.verifyElementText(findTestObject('usersTable/userName'), 'Player One')
WebUI.verifyElementText(findTestObject('usersTable/userEmail'), 'newplayer1@fmsqa.com')
WebUI.verifyElementText(findTestObject('usersTable/userFSRole'), 'Player')
WebUI.enhancedClick(findTestObject('accountUsersTab/trashIcon'))
WebUI.enhancedClick(findTestObject('accountUsersTab/deleteUser/confirmDeleteBtn'))
WebUI.verifyElementNotPresent(findTestObject('usersTable/deletedUserRow'), 0)
WebUI.closeBrowser()




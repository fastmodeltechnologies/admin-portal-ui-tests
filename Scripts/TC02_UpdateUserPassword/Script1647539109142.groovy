import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.URL)
WebUI.maximizeWindow()
WebUI.setText(findTestObject('SignInPage/emailTxtFld'), GlobalVariable.email)
WebUI.setText(findTestObject('SignInPage/passwordTxtFld'), GlobalVariable.password)
WebUI.enhancedClick(findTestObject('SignInPage/signINBtn'))
WebUI.delay(3)
WebUI.enhancedClick(findTestObject('usersTable/userEditIcon'))
WebUI.setEncryptedText(findTestObject('accountUsersTab/addNewUser/newAccountUser/passwordTxtFld'), 'YzGUhxdT33ALtOhrSF0KEQ==')
WebUI.setEncryptedText(findTestObject('accountUsersTab/addNewUser/newAccountUser/confirmPasswordTxtFld'), 'YzGUhxdT33ALtOhrSF0KEQ==')
WebUI.enhancedClick(findTestObject('accountUsersTab/addNewUser/newAccountUser/saveBtn'))
WebUI.delay(2)
WebUI.closeBrowser()
<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>140d1720-6102-4ef3-85ae-4a886f9067d4</testSuiteGuid>
   <testCaseLink>
      <guid>1ebe9fa4-0d0a-407d-8d90-a516fd0ab30d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC01_AddDelete_ConsumerPlayer to FS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cf0d771-ecef-4a23-88ca-f74dab18ca9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC03_EditAccountDetails</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9935756-1cb9-4df4-9856-2f1d8bce04d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC02_UpdateUserPassword</testCaseId>
   </testCaseLink>
</TestSuiteEntity>

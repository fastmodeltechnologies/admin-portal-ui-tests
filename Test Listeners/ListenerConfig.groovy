import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Properties.*
import java.lang.String
import com.kms.katalon.core.testdata.reader.ExcelFactory

class ListenerConfig {
	
	/**
	 * Executes before every test suite starts.
	 * @param testCaseContext related information of the executed test case.
	 */

	@BeforeTestCase
	def beforeTestSuite(TestSuiteContext testSuiteContext) {
		WebUI.comment("*************** Initializing Before TestSuite ***************")
		FileInputStream reader = new FileInputStream('./config.properties');
		Properties properties = new Properties();
		properties.load(reader);
		String environment = properties.getProperty('environment')
		String profile = properties.getProperty('profile')
		GlobalVariable.profile = profile
		GlobalVariable.environment = environment
		
switch (environment) {
			
		case "PROD":
		
			WebUI.comment("***** Launching URL : " +properties.getProperty('PRODURL') + "******");
			GlobalVariable.URL = properties.getProperty('PRODURL');
				
		switch (profile) {	
			
				case "PRODUCTION":
				WebUI.comment ("## PROFILE is PROD : PROD ##")
				GlobalVariable.email = "consumercoachprod@fmsqa.com"
				break;
		}
			
		break;
			
		case "STAGING":
		WebUI.comment("***** Launching URL : " +properties.getProperty('STAGINGURL') + "******");
		GlobalVariable.URL = properties.getProperty('STAGINGURL');
			
		switch (profile) {
			
		case "STAGINGQA":
		WebUI.comment ("## PROFILE is Staging : ##")
		GlobalVariable.email = "consumercoachstaging@fmsqa.com"
		break;
		}
		
		break;
		
		case "local":
		WebUI.comment("***** Launching URL : " +properties.getProperty('localURL') + "******");
		GlobalVariable.URL = properties.getProperty('localURL');
				
		switch (profile) {
					
		case "default":
		WebUI.comment ("## LocalHost: ##")
		GlobalVariable.email = "consumercoachstaging@fmsqa.com"
		break;
		
		}
     }		
	 	Object data = ExcelFactory.getExcelDataWithDefaultSheet("Data Files/Admin_testData.xlsx", environment , true)
		GlobalVariable.data = data
}

	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@AfterTestCase
	def afterTestCase(TestCaseContext testCaseContext) {
		String testCaseId = testCaseContext.getTestCaseId()
		String testCaseName = testCaseId.substring((testCaseId.lastIndexOf("/").toInteger()) + 1)
		WebUI.comment("###### Completed TestCase:  "+ testCaseName + " : "+testCaseContext.testCaseStatus+ "  #######")
		testCaseContext.getMessage()
	}

	/**
	 * Executes before every test suite starts.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	//@BeforeTestCase
	def beforeTestCase(TestCaseContext testCaseContext) {
		/*
		 * String testCaseId = testCaseContext.getTestCaseId() String testCaseName =
		 * testCaseId.substring((testCaseId.lastIndexOf("/").toInteger()) + 1)
		 * WebUI.comment("###### Starting TestCase:  "+ testCaseName + "  #######")
		 */
		}

	/**
	 * Executes after every test suite ends.
	 * @param testSuiteContext: related information of the executed test suite.
	 */
	@AfterTestSuite
	def afterTestSuite(TestSuiteContext testSuiteContext) {
		
	}
}